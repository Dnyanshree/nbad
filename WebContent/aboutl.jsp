<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>About Page</title>

<link href="css/common.css" rel="stylesheet" type="text/css">
</head>
<body>
<%@include file="header1.jsp" %>
<div id="main">
<div id="sidebar">
<table class="sidebartab" >
<tr class="sidebarRow" >
<td class="sidebarCol" >
Coins (<a href="">2</a>)
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
Participants (<a href="">2</a>)
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
Participation (<a href="" >2</a>)
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
&nbsp;
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
<a href="main.jsp">Home</a>
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
<a href="participate.jsp">Participate</a>
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
<a href="studies.jsp">My Studies</a>
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
<a href="recommend.jsp">Recommend</a>
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
<a href="contact.jsp">Contact</a>
</td>
</tr>
</table>
</div></div>
    <div class="howdiv">
        <p class="groove">
            <b>About us</b><br><br>
        
        Researchers Exchange Participations is a platform for researchers to exchange participations.
        <br><br>
        The aim of this platform is to encourage researchers to participate in each others user studies. 
        <br>
        Moreover, recruiting serious participants has always been a burden on a researcher's shoulder, 
        <br>thus, this platform gives researchers the opportunity to do that effectively and in a suitable
        <br>time.
        </p>
        </div>
<%@include file="footer.jsp" %>
</body>
</html>