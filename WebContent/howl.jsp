<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>How Page</title>
<link href="css/common.css" rel="stylesheet" type="text/css">
</head>
<body>
<%@include file="header1.jsp" %>
<div id="main">
<div id="sidebar">
<table class="sidebartab">
<tr class="sidebarRow" >
<td class="sidebarCol" >
Coins (<a href="">2</a>)
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
Participants (<a href="">2</a>)
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
Participation (<a href="" >2</a>)
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
&nbsp;
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
<a href="main.jsp">Home</a>
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
<a href="participate.jsp">Participate</a>
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
<a href="studies.jsp">My Studies</a>
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
<a href="recommend.jsp">Recommend</a>
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
<a href="contact.jsp">Contact</a>
</td>
</tr>
</table>
</div></div>
    <div class="howdiv">
        <p class="groove">
            <b> How it works</b><br><br>
        
            This site was built to help researchers conduct their user studies.<br><br>
        
        1 participation = 1 coin<br><br>
        
        <b>To participate</b>, go to "Participate" section and choose a study to complete.<br><br>
        
        <b> To get participants</b>, submit your study here to start getting Participations. In order to do <br>so, you must have enough coins in your account.
        </p>
        </div>
<%@include file="footer.jsp" %>
</body>
</html>