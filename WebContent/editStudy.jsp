<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>Edit Study</title>
<link href="css/common.css" rel="stylesheet" type="text/css">
</head>
<body>
<%@include file="header1.jsp" %>
<div id="Study">
Editing a study
</div>
<div id="eStudy">
<a href="studies.jsp">&lt;&lt; Back to Main Page</a>
</div>
<form id="formstyle" method="post" action="studies.jsp">
<div id="edit">
<label class="newlabel">Study Name* </label><input size="35" class="textstyle" type="text" name="StudyName" required><br><br>
<label class="newlabel">Question Text* </label><input size="35" class="textstyle"  type="text" name="QuesText" required><br><br>
<label class="newlabel">Image* </label><div id="imgdiv">
<img id="img"  src="http://www.logocollect.com/google/images/google_van_gogh.jpg" alt="http://www.logocollect.com/google/images/google_van_gogh.jpg">&nbsp;&nbsp;
<input class="uploadstyle"  type="button" value="Browse" name="upload">
</div>
<br><br>
<label class="newlabel"># Participants* </label><input size="35" class="textstyle"  type="text" name="numPart" required><br><br>
<label class="newlabel">Description* </label><textarea class="textstyle" rows="5" cols="36" required></textarea><br><br>
</div>
<div id="buttondiv">
<input  class="buttonstyle" type="submit" value="Update" name="update">
<br>
<br>
</div>

</form>
<%@include file="footer2.jsp" %>
</body>
</html>