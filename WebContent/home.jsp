
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <title>Home Page</title>
        
        <link href="css/common.css" rel="stylesheet" type="text/css">
    </head>
    <body>
    <%@ include file="/header.jsp" %>
      <div id="image">
          <img src="images/avatar.jpg" alt="images/avatar.jpg" class="center" />
      </div>
        <div>
            <%@ include file="/footer.jsp" %>
        </div>
    </body>
</html>
