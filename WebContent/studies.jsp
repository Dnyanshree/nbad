<!DOCTYPE html >
<html>
<head>
<title>Studies.jsp</title>

<link href="css/common.css" rel="stylesheet" type="text/css">
</head>
<body>
<%@include file="header1.jsp" %>
<div class="div_studies">
My Studies
</div>
<div id="div_mar">
<a href="newStudy.jsp">Add New Study</a>
<br>
<a href="main.jsp">&lt;&lt; Back to Main Page</a>
</div>
<div class="div_table">
<table class="tab_table">
<tr class="tr_table">
<th>Study Name</th>
<th>Requested Participants</th>
<th>Participations</th>
<th>Status</th>
<th>Action</th>
</tr>
<tr class="tr_field">
<td>GUI</td>
<td>10</td>
<td>3</td>
<td><input class="td_btn" type="button" value="Start"/></td>
<td><input class="td_btn" type="button" value="Edit" onClick="parent.location='http://localhost:8080/JSPWebsiteV2/editStudy.jsp'"/></td>
</tr>
<tr class="tr_field2">
<td>SEC</td>
<td>5</td>
<td>5</td>
<td><input class="td_btn" type="button" value="Stop"/></td>
<td><input class="td_btn" type="button" onClick="parent.location='http://localhost:8080/JSPWebsiteV2/editStudy.jsp'" value="Edit"/></td>
</tr>
<tr class="tr_field">
<td class="participate_font2">change font color</td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
<tr class="tr_field2">
<td class="participate_font1">change font color</td>
<td> </td>
<td> </td>
<td> </td>
<td> </td>
</tr>
</table>
</div>
<%@include file="footer2.jsp" %>
</body>
</html>