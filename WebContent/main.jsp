<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>Main Page</title>

<link href="css/common.css" rel="stylesheet" type="text/css">
</head>
<body>
<%@include file="header1.jsp" %>

<div id="main">
<div id="sidebar">
<table class="sidebartab">
<tr class="sidebarRow" >
<td class="sidebarCol" >
Coins (<a href="">2</a>)
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
Participants (<a href="">2</a>)
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
Participation (<a href="" >2</a>)
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
&nbsp;
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
<a href="/JSPWebsiteV5/UserController?action=main">Home</a>
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
<a href="participate.jsp">Participate</a>
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
<a href="studies.jsp">My Studies</a>
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
<a href="recommend.jsp">Recommend</a>
</td>
</tr>
<tr class="sidebarRow" >
<td class="sidebarCol" >
<a href="contact.jsp">Contact</a>
</td>
</tr>
</table>
</div>
</div>


<%@include file="footer.jsp" %>
</body>
</html>