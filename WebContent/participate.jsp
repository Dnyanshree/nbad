<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>Participate Page</title>

<link href="css/common.css" rel="stylesheet" type="text/css">
</head>
<body>
<%@include file="header.jsp"%>
	<div id="main">
		<div id="sidebar">
			<table class="sidebartab">
				<tr class="sidebarRow">
					<td class="sidebarCol">Coins (<a href="">2</a>)
					</td>
				</tr>
				<tr class="sidebarRow">
					<td class="sidebarCol">Participants (<a href="">2</a>)
					</td>
				</tr>
				<tr class="sidebarRow">
					<td class="sidebarCol">Participation (<a href="">2</a>)
					</td>
				</tr>
				<tr class="sidebarRow">
					<td class="sidebarCol">&nbsp;</td>
				</tr>
				<tr class="sidebarRow">
					<td class="sidebarCol"><a href="main.jsp">Home</a></td>
				</tr>
				<tr class="sidebarRow">
					<td class="sidebarCol"><a href="participate.jsp">Participate</a>
					</td>
				</tr>
				<tr class="sidebarRow">
					<td class="sidebarCol">My Questions</td>
				</tr>
				<tr class="sidebarRow">
					<td class="sidebarCol"><a href="recommend.jsp">Recommend</a>
					</td>
				</tr>
				<tr class="sidebarRow">
					<td class="sidebarCol"><a href="contact.jsp">Contact</a></td>
				</tr>
			</table>
		</div>
		<div id="page-wrap">
			<p class="parasidebar">Studies</p>
			<table id="tabparticipate">
				<tr class="participate_tr">
					<th>Study Name</th>
					<th>Image</th>
					<th>Question</th>
					<th>Action</th>
				</tr>
				<tr class="participate_tr1">
					<td>GUI</td>
					<td><img src="http://tinyurl.com/olk4e4z"
						alt="Image unavailable" id="img"></td>
					<td>3</td>
					<td><input id="buttonstle" type="button" value="Participate"
						onClick="parent.location='http://localhost:8080/JSPWebsiteV2/question.jsp'" /></td>
				</tr>
				<tr class="participate_tr2">
					<td>SEC</td>
					<td><img src="http://tinyurl.com/olk4e4z"
						alt="Image unavailable" id="img2"></td>
					<td>5</td>
					<td><input id="buttonstle2" type="button" value="Participate"
						onClick="parent.location='http://localhost:8080/JSPWebsiteV2/question.jsp'" /></td>
				</tr>
				<tr class="participate_tr1">
					<td class="participate_font2">change font color</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr class="participate_tr2">
					<td class="participate_font1">change font color</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
		</div>
	</div>

	<%@include file="footer.jsp"%>
</body>
</html>