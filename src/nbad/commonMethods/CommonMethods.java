package nbad.commonMethods;
import java.net.MalformedURLException;

import nbad.model.User;
import nbad.utility.UserDB;

public class CommonMethods {
	public static User ValidateUser(String username,String password) throws MalformedURLException
	{
		System.out.println("inside ValidateUser : "+ username + " \n "+ password);
		User returnUser = UserDB.getUser(username);
		
		if(returnUser != null && UserDB.checkPassword(username,password))
		{
			return returnUser;
		}
		else
		{
				return null;
		}
	}

	public static User SignUpUser(String name, String email, String password) {
		
		System.out.println("Inside SignUp");
		
		User returnUser = new User();
		returnUser.setCoins(0);
		returnUser.setParticipants(0);
		returnUser.setParticipation(0);
		returnUser.setEmail(email);
		returnUser.setName(name);

		String record = name+","+email+","+password+";";
		
		
		
		/*
		 * insert a record for the user in the table
		 * fetch user information for the user and store it in returnUser
		 */
		
		if(UserDB.signUpUser(record))
		return returnUser;
		return null;
	}
		
	
	
		}
