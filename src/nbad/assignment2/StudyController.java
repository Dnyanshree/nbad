package nbad.assignment2;

import java.io.IOException;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nbad.assignment2.StudyController;
import nbad.assignment2.UserController;
import nbad.model.User;
import nbad.model.Answer;
import nbad.model.Study;

/**
 * Servlet implementation class StudyController
 */
@WebServlet("/StudyController")
public class StudyController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static String serverName = "";
	public static URL UserFilePath;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudyController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		serverName = request.getServerName();
		String action = request.getParameter("action");
		String url = "/home.jsp";
		HttpSession session = request.getSession();
		User user;
		Study study;
		/* 
		 * 
Checks the http request for a parameter called �action�
 If there is no action parameter, or if it has an unknown value
		Checks the session for �theUser� object,
		 If it exists
			Dispatches to the website�s main page (main.jsp)
		 If it does not exist
		 	Dispatches to the website�s main page (home.jsp)*/
		if (action == null) {
			session = request.getSession();
			if ((user = (User) session.getAttribute("theUser")) != null) {
				url = "/main.jsp";
			} else {
				url = "/home.jsp";
			}
		}
		/*
 If action is �participate�
		    Checks the session for �theUser� object,
		 If it exists
		    Checks the http request object for parameter called �StudyCode�
		 		If does not exist
		 			Retrieves the open studies (status = start) from the DB.
					Puts them in the http request object.
		 			Dispatches to the pariticipate.jsp.
		 		If it exists
		 			Retrieves the study record (question) from the DB.
					Puts it in the http request object.
					Dispatches to the question.jsp.
		 If it does not exist
		 	Dispatches to the login page (login.jsp)*/
		else if (action.equalsIgnoreCase("participate")) {
			session = request.getSession();
			if ((user = (User) session.getAttribute("theUser")) != null) {
				String studyCode = request.getParameter("code").toString();
				if (!(boolean) request.getAttribute(studyCode)) {
					/*
					 * Retrieves the open studies (status = start) from the DB.
					 * Puts them in the http request object
					 */
					url = "/participate.jsp";
				} else {
					String question = request.getParameter("question").toString();
					request.setAttribute(question, question);
					url = "/question.jsp";
				}
			} 
			else {
			url = "/login.jsp";
		}
	}
		/*
 If action is �edit�
		 	Checks the session for �theUser� object,
		 If it exists
		 	Checks the http request object for a parameter called �StudyCode�.
		 	Pulls a study record from the DB based on the study code and creator�s email address.
			Adds it to the http request object.
			Dispatches to the editstudy.jsp.
		 If it does not exist
		 	Dispatches to the login page (login.jsp)*/
		else if (action.equalsIgnoreCase("edit")) {
			session = request.getSession();
			if ((user = (User) session.getAttribute("theUser")) != null) {
				String studyCode = request.getParameter("code").toString();
				if ((boolean) request.getAttribute(studyCode)) {
					/*
					 * Pulls a study record from the DB based on the study code
					 * and creator�s email address. Adds it to the http request
					 * object.
					 */
					url = "/editSudy.jsp";
				}
			} else {
				url = "/login.jsp";
			}
		}
		 	
/* If action is �update�
		 	Checks the session for �theUser� object,
		 If it exists
		 	Checks the http request for parameter called �study�
		    Updates the study record in the DB.
			Retrieves the user�s studies from the DB.
			Puts them in the http request object.
			Dispatches to the studies.jsp
		 If it does not exist
		 	Dispatches to the login page (login.jsp)*/
		else if (action.equalsIgnoreCase("update")) {
			session = request.getSession();
			if ((user = (User) session.getAttribute("theUser")) != null) {
				String Study = request.getParameter("study").toString();
				if ((boolean) request.getAttribute(Study)) {
					/*
					 *  Updates the study record in the DB.
						Retrieves the user�s studies from the DB.
						Puts them in the http request object.
					 */
					url = "/studies.jsp";
				}
			} else {
				url = "/login.jsp";
			}
		} 	
		 			 	
 /*If action is �add�
		 	Checks the session for �theUser� object,
		 If it exists
		 	Checks the http request for parameter called �study�.
		    Adds the study record to the DB.
		 	Retrieves the user�s studies from the DB.
			Puts them in the http request object.
			Dispatches to the studies.jsp*/
		else if (action.equalsIgnoreCase("add")) {
			session = request.getSession();
			if ((user = (User) session.getAttribute("theUser")) != null) {
				String Study = request.getParameter("study").toString();
				if ((boolean) request.getAttribute(Study)) {
					/*
					 *  Add the study record in the DB.
						Retrieves the user�s studies from the DB.
						Puts them in the http request object.
					 */
					url = "/studies.jsp";
				}
			} else {
				url = "/login.jsp";
			}
		} 
		
 /*If action is �start�
		 	Checks the session for �theUser� object.
		 If it exists
		 	Checks the http request for parameter called �StudyCode�.
		 	Sets the study status to start.
			Retrieves the user�s studies from the DB.
			Puts them in the http request object.
			Dispatches to the studies.jsp.
		 If it does not exist
		 	Dispatches to the login page (login.jsp)*/
		else if (action.equalsIgnoreCase("start")) {
			session = request.getSession();
			if ((user = (User) session.getAttribute("theUser")) != null) {
				String studyCode = request.getParameter("code").toString();
				if ((boolean) request.getAttribute(studyCode)) {
					/*
					 *  Sets the study status to start.
						Retrieves the user�s studies from the DB.
						Puts them in the http request object.
					 */
					url = "/studies.jsp";
				}
			} else {
				url = "/login.jsp";
			}
		} 	
		
		
 /*If action is �stop�
		 	Checks the session for �theUser� object,
		 If it exists
		 	Checks the http request for parameter called �StudyCode�.	
		 	Sets the study status to stop.
			Retrieves the user�s studies from the DB.
			Puts them in the http request object.
			Dispatches to the studies.jsp
		 If it does not exist
		 	Dispatches to the login page (login.jsp)*/
		else if (action.equalsIgnoreCase("stop")) {
			session = request.getSession();
			if ((user = (User) session.getAttribute("theUser")) != null) {
				String studyCode = request.getParameter("code").toString();
				if ((boolean) request.getAttribute(studyCode)) {
					/*
					 *  Sets the study status to stop.
						Retrieves the user�s studies from the DB.
						Puts them in the http request object.
					 */
					url = "/studies.jsp";
				}
			} else {
				url = "/login.jsp";
			}
		} 		
		
/* If action is �answer�
		 	Checks the session for �theUser� object,
		 If it exists
		 	Checks the http request for parameter called �StudyCode� and �choice�.
			Adds the answer to the DB.
			Update the user data (coins and participation in the DB and the session).
		    Retrieves the open studies (status = start) from the DB.
			Puts them in the http request object.
			Dispatches to the pariticipate.jsp.
		 If it does not exist
		 	Dispatches to the login page (login.jsp)*/
	
		else if (action.equalsIgnoreCase("answer")) {
			session = request.getSession();
			if ((user = (User) session.getAttribute("theUser")) != null) {
				String studyCode = request.getParameter("code").toString();
				String Choice = request.getParameter("choice").toString();
				if ((boolean) request.getAttribute(studyCode) && (boolean) request.getAttribute(Choice)) {
					/*
					 * Adds the answer to the DB.
			Update the user data (coins and participation in the DB and the session).
		    Retrieves the open studies (status = start) from the DB.
			Puts them in the http request object.
					 */
					url = "/participate.jsp";
				}
			} else {
				url = "/login.jsp";
			}
		} 	
		
		
	getServletContext().getRequestDispatcher(url).forward(request, response);	
	}

}
