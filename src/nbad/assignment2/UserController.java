package nbad.assignment2;

import java.io.IOException;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nbad.commonMethods.CommonMethods;
import nbad.model.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/UserController")
public class UserController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static String serverName = "";
	public static URL UserFilePath;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserController() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		serverName = request.getServerName();
		String action = request.getParameter("action");
		String url = "/home.jsp";
		HttpSession session = request.getSession();
		User user;

		/*
		 * If there is no action parameter, or if it has an unknown value,
		 * dispatches directly to the website�s main page (home.jsp).
		 */
		if (action == null) {
			url = "/home.jsp";
		} else if (action.equalsIgnoreCase("login")) {
			
			/*
			 * If action is �login� >Checks the http request for parameters
			 * called: email and password >Validates if the combination exists
			 * in the database >If they are correct, >Creates a User bean for
			 * the user >Adds the User bean to the current session as �theUser�.
			 * >Dispatches to the main.jsp >If they are not correct, >Adds an
			 * error message to the http request object , call the parameter
			 * �msg�. >Dispatches to the login.jsp page.
			 */

			String username = request.getParameter("username").toString();
			String password = request.getParameter("password").toString();

			System.out.println(username);
			System.out.println(password);

			user = new User();

			if ((user = CommonMethods.ValidateUser(username, password)) != null) {
				session.setAttribute("theUser", user);
				request.setAttribute("msg", "Login Successful!");
				url = "/main.jsp";
			} else {
				request.setAttribute("msg", "Invalid Credentials");
				url = "/login.jsp";
			}
		} else if (action.equalsIgnoreCase("create")) {

			/*
			 * If action is �create� >Checks the http request for parameters:
			 * name, email, password, and confirm password. >Validates the above
			 * information for possible errors. >If there is any error: >Adds an
			 * error message to the http request object, call the parameter
			 * �msg�. >Adds the above information to the http request.
			 * >Dispatches to the signup.jsp page. >If there is no errors
			 * >Creates a User bean for the user >Adds the User bean to the
			 * current session as �theUser� >dispatches to the main.jsp.
			 */

			System.out.println("Inside Create");
			user = new User();

			String name = request.getParameter("name").toString();
			String email = request.getParameter("email").toString();
			String password = request.getParameter("password").toString();
			String cpassword = request.getParameter("confpassword").toString();

			System.out.println(name + "," + email + "," + password + ";");

			if (name != null && email != null && password != null && cpassword != null && password.equals(cpassword)) {
				
				user = CommonMethods.SignUpUser(name, email, password);

				if (user != null) {
					request.setAttribute("msg", "Registration successful");
					session.setAttribute("theUser", user);
					url = "/main.jsp";
				} else {
					request.setAttribute("msg", "Registration failed");
					url = "/signup.jsp";
				}
			} else {
				System.out.println("NULL values received by servlet.");
				request.setAttribute("msg", "Registration failed");
				url = "/signup.jsp";
			}

		} else if (action.equalsIgnoreCase("how")) {
			/*
			 * If action is �how� >Checks the session for �theUser� object, >If
			 * it exists >Dispatches to main.jsp >if it does not exist
			 * >Dispatches to how.jsp
			 */
			session = request.getSession();
			if ((user = (User) session.getAttribute("theUser")) != null) {
				url = "/howl.jsp";
			} else {
				url = "/how.jsp";
			}

		} else if (action.equalsIgnoreCase("about")) {
			/*
			 * If action is �about� >Checks the session for �theUser� object,
			 * >If it exists >Dispatches to about.jsp >If it does not exist
			 * >Dispatches to aboutl.jsp
			 */
			session = request.getSession();
			if ((user = (User) session.getAttribute("theUser")) != null) {
				url = "/aboutl.jsp";
			} else {
				url = "/about.jsp";
			}
		} else if (action.equalsIgnoreCase("home")) {
			/*
			 * If action is �home� Checks the session for �theUser� object, If
			 * it exists Dispatches to main.jsp If it does not exist Dispatches
			 * to home.jsp
			 */
			session = request.getSession();
			if ((user = (User) session.getAttribute("theUser")) != null) {
				url = "/main.jsp";
			} else {
				url = "/home.jsp";
			}
		} else if (action.equalsIgnoreCase("main")) {
			/*
			 * If action is �main� Checks the session for �theUser� object, If
			 * it exists Dispatches to main.jsp If it does not exist Dispatches
			 * to login.jsp
			 */
			session = request.getSession();
			if ((user = (User) session.getAttribute("theUser")) != null) {
				url = "/main.jsp";
			} else {
				url = "/login.jsp";
			}

		}
		getServletContext().getRequestDispatcher(url).forward(request, response);
	}

}
