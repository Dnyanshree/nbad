package nbad.model;

import java.io.Serializable;

/*
 * User
 Name
 Email
 Coins
 Participants : how many studies have he/she created
 Participation : how many studies have he/she participated in
1 participation = 1 coin
1 participant = 1 coin
 */

public class User implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String email;
	private int coins;
	private int participants;
	private int participation;
	
	
	
	
	public User() {
		super();
		name="";
		email="";
		coins=0;
		participants=0;
		participation=0;
		
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getCoins() {
		return coins;
	}
	public void setCoins(int coins) {
		this.coins = coins;
	}
	public int getParticipants() {
		return participants;
	}
	public void setParticipants(int participants) {
		this.participants = participants;
	}
	public int getParticipation() {
		return participation;
	}
	public void setParticipation(int participation) {
		this.participation = participation;
	}
	
	
	
}
