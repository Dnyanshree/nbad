/**
 * 
 */
package nbad.model;

import java.io.Serializable;
import java.util.*;


/*
 Name
 Code
 DateCreated
 Email (Creator)
 Question
 String getImageURL() � URL that can be used in your pages, pointing to an image file 
 						within the project for your question. Generated from study code.
 Requestedparticipants : How many participants does the creator want
 Numofparticipants : How participants have finished this study thus far.
 Description
 Status
 List / Collection (your choice) of answer.
 */
public class Study implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String code;
	private String dateCreated;
	private String email;
	private String question;
	private String requestedParticipants;
	private String numOfParticipants;
	private String description;
	private String status;
	private ArrayList<Answer> answers;
	/**
	 * 
	 */
	public Study() {
		super();
		name="";
		code="";
		dateCreated="";
		email="";
		question="";
		requestedParticipants="";
		numOfParticipants="";
		description="";
		status="";
	}
	
	public String getImageURL(){
		String URL = "/JSPWebsiteV5/WebContent/images/tree.png/";
		return URL;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getRequestedParticipants() {
		return requestedParticipants;
	}
	public void setRequestedParticipants(String requestedParticipants) {
		this.requestedParticipants = requestedParticipants;
	}
	public String getNumOfParticipants() {
		return numOfParticipants;
	}
	public void setNumOfParticipants(String numOfParticipants) {
		this.numOfParticipants = numOfParticipants;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(ArrayList<Answer> answers) {
		this.answers = answers;
	}
	
	
}
