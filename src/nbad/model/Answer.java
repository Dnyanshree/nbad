package nbad.model;

import java.io.Serializable;

/**
 * 
 */

public class Answer implements Serializable {
	private static final long serialVersionUID = 1L;
       
	private String email;
	private int choice;
	
	public int getAverage(){
		return choice;
		
	}
	
	public int getMinimum(){
		return choice;
		
	}
	public int getMaximum(){
		return choice;
		
	}
	
	public int getSD(){
		return choice;
		
	}
    public Answer() {
        super();
        // TODO Auto-generated constructor stub
    }

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getChoice() {
		return choice;
	}

	public void setChoice(int choice) {
		this.choice = choice;
	}

	


}
