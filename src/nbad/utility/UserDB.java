package nbad.utility;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import nbad.assignment2.UserController;
import nbad.model.User;

public class UserDB {

	/*
	 * Hard-Coded set of user details (your choice on how to represent) User
	 * getUser(String email) – returns a user object based on the email.
	 * List/Collection<User> getUsers() – returns a set of all the users in the
	 * hardcoded “database”
	 */

	public static boolean checkPassword(String email, String password) {
		StringBuilder sb = new StringBuilder();
		
		try {
			URL url = new URL("http://" + UserController.serverName + ":8080/JSPWebsiteV5/resources/UserFile");
		
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
			String line = "";

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String[] usrArr = sb.toString().split(";");

		for (String user : usrArr) {
			if (user.split(",")[1].equals(email) && user.split(",")[2].equals(password)) {
				return true;
			}
		}
		return false;
	}

	public static ArrayList<User> getUsers() {
		ArrayList<User> userList = new ArrayList<User>();
		StringBuilder sb = new StringBuilder();
		URL url = UserDB.class
				.getResource("http://" + UserController.serverName + ":8080/JSPWebsiteV5/resources/UserFile");

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
			String line = "";

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String[] usrArr = sb.toString().split(";");

		for (String user : usrArr) {
			User usr = new User();
			usr.setName(user.split(",")[0]);
			usr.setEmail(user.split(",")[1]);
			usr.setCoins(0);
			usr.setParticipants(0);
			usr.setParticipation(0);

			userList.add(usr);
		}

		return userList;
	}

	public static User getUser(String email) throws MalformedURLException {

		User user = new User();
		URL url = new URL("http://" + UserController.serverName + ":8080/JSPWebsiteV5/resources/UserFile");
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
			String line = "";

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println(sb.toString());

		String[] usrArr = sb.toString().split(";");

		for (String usr : usrArr) {
			System.out.println(usr);
			if (usr.split(",")[1].equals(email)) {
				user.setCoins(0);
				user.setParticipants(0);
				user.setName(usr.split(",")[0]);
				user.setEmail(usr.split(",")[1]);
			}
		}

		return user;
	}
	
	public static boolean signUpUser(String record)
	{
		File f = new File("UserFile");
		try {
			BufferedWriter wr = new BufferedWriter(new FileWriter(f.getAbsoluteFile()));
			wr.write(record);
			wr.close();
			return true;

		} catch (Exception e1) {
		try {
			URL url = new URL("http://" + UserController.serverName + ":8080/JSPWebsiteV5/resources/UserFile");

			URLConnection urlConn = url.openConnection();
			urlConn.setDoOutput(true);
			urlConn.setDoInput(true);
			urlConn.setUseCaches(true);
			urlConn.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
			
			DataOutputStream dos = new DataOutputStream (urlConn.getOutputStream()); 
			System.out.println(record);
			dos.writeBytes(record);
			dos.flush(); 
		    dos.close();
		    
		    saveState(record);
			return true;
			
		} catch (IOException e) {
			e.printStackTrace();
		} 
		
		}
		return false;
	}
	
	 public static void saveState(String record) {                                          
		    FileWriter fileWriter = null;
		    PrintWriter printWriter = null;
		    try {                                                            
		      fileWriter = new FileWriter("UserFile");
		      printWriter = new PrintWriter(fileWriter);         
		      printWriter.println(record);                                  
		      printWriter.close();
		      return;                                                        
		    }                                                                
		    catch (IOException e) {
		    }
		  }
}
